var express = require('express')
var archivo = require('../archivos')
var drive = require('./driver')

var router = express.Router()

router.get('/driver', function (req,res) {
	archivo(req,res,'drive/index.html')
})
router.get('/maps', function (req,res) {
	archivo(req,res,'maps/index.html')
})

router.get('/datdriver', function (req,res) {
	drive.datdv(function (dato) {
		res.json(dato)
	})
})

module.exports = router