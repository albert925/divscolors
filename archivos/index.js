var path = require ('path')

module.exports = function (req,res,name) {
	var file = path.join(__dirname, '..', 'public/' + name)
	res.sendFile(file)
}