var $ = require('jquery')
var acc = require('./acciones')
var jshtml = require('./htmljs')

$(inicio_pagina)

function inicio_pagina () {
	$('.cajas').html(jshtml.divs)
	$('.cajas div').mousemove(acc.colorear)
	//googlempas()
}

$(function(){
	var show = 1;
	var this_right = $("#chat").width();
	$("#hide-show").css({right:this_right+"px"});
	$("#hide-show").click(function(){
		//Hide
		if (show == 1){
			show = 0;	
			$(this).text("Show");
			$("#chat").hide();
			$(this).css({right:"0"});
		}else{
			show = 1;
			$(this).text("Hide");
			$("#chat").show();
			$(this).css({right:this_right});
		}
		
	});
});
